package edu.sjsu.android.gradebook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import edu.sjsu.android.gradebook.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    private final String AUTHORITY = "edu.sjsu.android.data_provider";
    private final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        binding.add.setOnClickListener(this::addStudent);
        binding.get.setOnClickListener(this::getAllStudents);
    }

    public void addStudent(View view) {
        ContentValues values = new ContentValues();
        values.put("name", binding.studentName.getText().toString());
        values.put("grade", binding.studentGrade.getText().toString());
        //Toast message if successfully inserted
        if (getContentResolver().insert(CONTENT_URI, values) != null) {
            Toast.makeText(this, "Student Added", Toast.LENGTH_SHORT).show();
        }

    }

    public void getAllStudents(View view) {
        /**
         * Sort student by name
         *
         * use getContentResolver() to communicate with StudentsProvider identified by Uri.
         *
         * The query method in StudentsProvider returns a Cursor object which is used to get data from a database row by row.
         *
         * To call moveToFirst to read from the first row (the method returns true if moves successfully), and display
         * each row until there is no next row to move to.
         *
         *
         * getAllStudent is the method that handles a click on "Retrieve Students" button
         *
         *
         *
         */

        try (Cursor c = getContentResolver().query(CONTENT_URI, null, null, null, "name")) {

            if (c.moveToFirst()) {
                String result = "Thomas Lao's Gradebook: \n";
                do {
                    for (int i = 0; i < c.getColumnCount(); i++) {
                        result = result.concat(c.getString(i) + "\t");
                    }
                    result = result.concat("\n");

                } while (c.moveToNext());
                binding.result.setText(result);
            }
        }
    }
}
