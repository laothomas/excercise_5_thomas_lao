package edu.sjsu.android.data_provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

/**
 * Makes it easier for StudentProvider implementations, especially for creating/upgrading the database so the database wll not be blocked by
 * long running database upgrades
 */
public class StudentDB extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "studentsDatabase";
    private static final int VERSION = 1;
    private static final String TABLE_NAME = "students";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String GRADE = "grade";
    static final String CREATE_TABLE =
            " CREATE TABLE " + TABLE_NAME +
                    " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + NAME + " TEXT NOT NULL, "
                    + GRADE + " TEXT NOT NULL);";

    public StudentDB(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        /**
         * HERE JUST CREATE AN EMPTY TABLE
         */
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    /**
     * Called when the database needs to be upgraded.
     * upgrading the database deletes the old table and creates a new table
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        /**
         * drop the old table and make a new table
         */
        onCreate(sqLiteDatabase);
    }

    /**
     * call this method every time we want to write to a database
     */
    public long insert(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        return database.insert(TABLE_NAME, null, contentValues);
    }

    public Cursor getAllStudents(String orderBy) {
        SQLiteDatabase database = getWritableDatabase();
        return database.query(TABLE_NAME, new String[]{ID, NAME, GRADE}, null, null, null, null, orderBy);
    }


}
